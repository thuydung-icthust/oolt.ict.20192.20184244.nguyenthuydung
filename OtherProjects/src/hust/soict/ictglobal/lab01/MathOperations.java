package hust.soict.ictglobal.lab01;
import javax.swing.JOptionPane;

public class MathOperations {
    public static void main(String[] avgs) {
        String strNum1, strNum2;
        String strNotification = "You've just entered ";
        double sum, diff, product, quotient;

        strNum1 = JOptionPane.showInputDialog(null, "Please input the first number: ", "Input the first number",
                JOptionPane.INFORMATION_MESSAGE);
        strNotification += strNum1 + " and ";
        strNum2 = JOptionPane.showInputDialog(null, "Please input the second number: ", "Input the second number",
                JOptionPane.INFORMATION_MESSAGE);
        strNotification += strNum2;

        // JOptionPane.showMessageDialog(null, strNotification, " Show two numbers",
        // JOptionPane.INFORMATION_MESSAGE);
        sum = Double.parseDouble(strNum1) + Double.parseDouble(strNum2);
        diff = Double.parseDouble(strNum1) - Double.parseDouble(strNum2);
        product = Double.parseDouble(strNum1) * Double.parseDouble(strNum2);
        quotient = Double.parseDouble(strNum1) * Double.parseDouble(strNum2);
        JOptionPane.showInternalMessageDialog(null, " Sum is: " + sum + "\n Different is: " + diff + "\n Product is: "
                + product + "\n Quotient is: " + quotient, "Show results", JOptionPane.INFORMATION_MESSAGE);

        System.exit(0);
    }
}