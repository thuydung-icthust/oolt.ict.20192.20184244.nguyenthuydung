package hust.soict.ictglobal.lab01;

import javax.swing.*;

public class Lab01_exer6 {
    public static void main(String[] args) {
        FirstDegreeEquation eqn = new FirstDegreeEquation();
        eqn.Solve1();

        SystemOfFirstDegreeEquations eqn2 = new SystemOfFirstDegreeEquations();
        eqn2.Solve2();

        SecondDegreeEquation second_eqn = new SecondDegreeEquation();
        second_eqn.Solve();

    }
}

class FirstDegreeEquation {
    String a, b;
    double c, d, e;

    void Solve1() {
        JTextField xField = new JTextField(6);
        JTextField yField = new JTextField(6);

        JPanel myPanel = new JPanel();
        myPanel.add(new JLabel("a:"));
        myPanel.add(xField);
        myPanel.add(Box.createHorizontalStrut(15)); // a spacer
        myPanel.add(new JLabel("b:"));
        myPanel.add(yField);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter the constants a and b of the equation ax + b = 0.", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            a = xField.getText();
            b = yField.getText();
            d = Double.parseDouble(a);
            e = Double.parseDouble(b);
            c = -e / d;

            JOptionPane.showInternalMessageDialog(null, "The solution is: " + c, "1-degree eqn",
                    JOptionPane.INFORMATION_MESSAGE);
            // System.exit(0);
        }
    }
}

class SystemOfFirstDegreeEquations {
    String a11, a12, a21, a22, b1, b2;
    double X1, X2, A11, A12, A21, A22, B1, B2, D, D1, D2;

    void Solve2() {
        JTextField a11Field = new JTextField(6);
        JTextField a12Field = new JTextField(6);
        JTextField a21Field = new JTextField(6);
        JTextField a22Field = new JTextField(6);
        JTextField b1Field = new JTextField(6);
        JTextField b2Field = new JTextField(6);

        JPanel myPanel = new JPanel();
        myPanel.add(new JLabel("a11:"));
        myPanel.add(a11Field);
        myPanel.add(Box.createHorizontalStrut(15));
        // a spacer
        myPanel.add(new JLabel("a12:"));
        myPanel.add(a12Field);
        myPanel.add(Box.createHorizontalStrut(15));

        myPanel.add(new JLabel("a21:"));
        myPanel.add(a21Field);
        myPanel.add(Box.createHorizontalStrut(15));

        myPanel.add(new JLabel("a22:"));
        myPanel.add(a22Field);
        myPanel.add(Box.createHorizontalStrut(15));

        myPanel.add(new JLabel("b1:"));
        myPanel.add(b1Field);
        myPanel.add(Box.createHorizontalStrut(15));

        myPanel.add(new JLabel("b2:"));
        myPanel.add(b2Field);
        myPanel.add(Box.createHorizontalStrut(15));

        int result = JOptionPane.showConfirmDialog(null, myPanel, "Please Enter the coefficients.",
                JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            a11 = a11Field.getText();
            a12 = a12Field.getText();
            a21 = a21Field.getText();
            a22 = a22Field.getText();
            b1 = b1Field.getText();
            b2 = b2Field.getText();

            A11 = Double.parseDouble(a11);
            A12 = Double.parseDouble(a12);
            A21 = Double.parseDouble(a21);
            A22 = Double.parseDouble(a22);
            B1 = Double.parseDouble(b1);
            B2 = Double.parseDouble(b2);

            D = A11 * A22 - A12 * A21;
            D1 = B1 * A22 - B2 * A12;
            D2 = A11 * B2 - A21 * B1;

            if (D != 0) {
                X1 = D1 / D;
                X2 = D2 / D;
                JOptionPane.showInternalMessageDialog(null,
                        "The solutions (x1, x2) is: \n x1 = " + X1 + "\n x2 = " + X2, "system of 1-degree eqn",
                        JOptionPane.INFORMATION_MESSAGE);
                // System.exit(0);
            } else {
                if (D1 == D2 && D1 == 0) {
                    JOptionPane.showInternalMessageDialog(null, "The system has infinitely many solutions",
                            "system of 1-degree eqn", JOptionPane.INFORMATION_MESSAGE);
                    // System.exit(0);
                } else {
                    JOptionPane.showInternalMessageDialog(null, "The system has no solution.", "system of 1-degree eqn",
                            JOptionPane.INFORMATION_MESSAGE);
                    // System.exit(0);
                }

            }

        }

    }
}

class SecondDegreeEquation {
    String a, b, c;
    Double x1, x2, delta, A, B, C;

    void Solve() {
        JTextField xField = new JTextField(6);
        JTextField yField = new JTextField(6);
        JTextField zField = new JTextField(6);

        JPanel myPanel = new JPanel();
        myPanel.add(new JLabel("a:"));
        myPanel.add(xField);
        myPanel.add(Box.createHorizontalStrut(15)); // a spacer
        myPanel.add(new JLabel("b:"));
        myPanel.add(yField);
        myPanel.add(Box.createHorizontalStrut(15)); // a spacer
        myPanel.add(new JLabel("c:"));
        myPanel.add(zField);

        int result = JOptionPane.showConfirmDialog(null, myPanel,
                "Please Enter the constants a and b of the equation ax^2 + bx + c = 0.", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            a = xField.getText();
            b = yField.getText();
            c = zField.getText();
            A = Double.parseDouble(a);
            B = Double.parseDouble(b);
            C = Double.parseDouble(c);

            delta = B * B - 4 * A * C;
            if (delta < 0) {
                JOptionPane.showInternalMessageDialog(null, "No solution.", null, JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }

            else if (delta == 0) {
                x1 = -B / (2 * A);

                JOptionPane.showInternalMessageDialog(null, "The solution is: " + x1, "2-degree eqn",
                        JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }

            else {
                x1 = (-B + Math.sqrt(delta)) / (2 * A);
                x2 = (-B - Math.sqrt(delta)) / (2 * A);

                JOptionPane.showInternalMessageDialog(null, "The solution are: " + x1 + " and " + x2, "2-degree eqn",
                        JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);

            }

        }

    }

}