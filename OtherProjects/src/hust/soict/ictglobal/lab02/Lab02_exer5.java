package hust.soict.ictglobal.lab02;
import java.util.Scanner;


public class Lab02_exer5 {
	//Scanner keyboard = new Scanner(System.in);

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		// TODO Auto-generated method stub
		
		System.out.println("Input month: ");
		
		//scanner.useDelimiter("/");
		//String month = scanner.next();
		int month = scanner.nextInt();
		while(month <= 1 || month >= 13) {
			System.out.println("Invalid month, please re-enter: ");
			month = scanner.nextInt();
		}
		System.out.println("Input year: ");
		int year = scanner.nextInt();

		System.out.println(month);
		System.out.println(year);

		System.out.printf("This month has (%d) days. \n", new CheckMonth().check(month, year));
		


	}

	
	

}


class CheckMonth {
	int check(int month, int year) {
		if (month == 2) {
			if(year % 4 == 0 && year % 1000 != 0) 
			return 29;
			return 28;
		}

		switch (month) {
			case 1: case 3: case 5: case 7: case 8: case 10: case 12:
				return 31;
				//break;
			case 4: case 6: case 9: case 11:
				return 30;
		}


		return -1;
	}
}