package hust.soict.ictglobal.lab02;
import java.util.Scanner;


public class Lab02_exer4 {
	//Scanner keyboard = new Scanner(System.in);

	public static void main(String[] args) {
		
		Scanner keyboard = new Scanner(System.in);
		// TODO Auto-generated method stub
		
		System.out.println("Input integer n: ");
		int n = keyboard.nextInt();
		//System.out.println(n);
		
		int MAX_STARS = (n-1)*2 +1;
		//System.out.println(MAX_STARS);
		for(int i = 0; i< n; i++)
		{
			for(int j = -MAX_STARS/2; j<= MAX_STARS /2; j++) {
				if(Math.abs(j) <= i)
					System.out.print("*");
				else System.out.print(" ");
				
			}
			System.out.print("\n");
		}
		
		

	}


}
