package hust.soict.ictglobal.lab02;
import java.util.Arrays;

class Matrices {
	int[][] AddMatrices(int[][] matrix1, int [][] matrix2) {
		
		int row = matrix1.length;
		int col = matrix1[0].length;
		int rel[][] = new int[row][col];
		//System.out.printf("row = %d, col = %d.\n", row, col);
		
		for(int i = 0; i< row; i++)
			for(int j = 0; j< col; j++) {
				rel[i][j] = matrix1[i][j] + matrix2[i][j];
			}
		
		return rel;
		
	}
	
	void PrintResult(int[][] matrix1, int[][] matrix2, int[][] result) {
		result = AddMatrices(matrix1, matrix2);
		for(int i = 0; i < matrix1.length; i++)
			System.out.println(Arrays.toString(matrix1[i]));
		System.out.println("+");
		for(int i = 0; i < matrix2.length; i++)
			System.out.println(Arrays.toString(matrix2[i]));
		System.out.println("=");
		for(int i = 0; i < matrix1.length; i++)
			System.out.println(Arrays.toString(result[i]));
	}
}

public class Lab02_exer7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [][] matrix1 = {{ 1, 2, 3, 4},
				{3, 4, 5, 6},
				{4, 5, 2, 1}
		};
		
	int[][] matrix2 = { {2, 2, 2, 2},
			{3, 3, 3, 3},
			{1, 1, 1, 1}
	};
	
	int [][] result = {{0}};
	Matrices matrix = new Matrices();
	matrix.PrintResult(matrix1, matrix2, result);
	//result = matrix.AddMatrices(matrix1, matrix2);
	//for(int i = 0; i<(matrix1.length); i++)
	//	System.out.println(Arrays.toString(result[i]));
	

	}

}
