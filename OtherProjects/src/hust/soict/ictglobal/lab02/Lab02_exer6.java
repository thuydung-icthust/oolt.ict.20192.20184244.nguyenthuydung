package hust.soict.ictglobal.lab02;
import java.util.Arrays;

//import com.sun.tools.javac.code.Attribute.Array;

//import com.sun.tools.classfile.StackMapTable_attribute.verification_type_info;

class Sort {
	int[] InsertionSort(int arr[])
	{
		int len = arr.length;
		for(int i = 1; i < len; i++) {
			int v = arr[i];
			int j = i - 1;
			while(j >= 0 && arr[j] > v) {
				arr[j+1] = arr[j];
				j--;
				arr[j+1] = v;
			}
			
			
		}
		return arr;
	}
	int CalculateSum(int arr[]) {
		int len = arr.length;
		int sum = 0;
		for(int i = 0; i< len; i++) {
			sum+= arr[i];
		}
		
		return sum;
	}
	
	float FindAverage(int arr[]) {
		int sum = CalculateSum(arr);
		float avg = 0;
		avg = (float)sum / (arr.length);
		return avg;
		
	}
}

public class Lab02_exer6 {
	
	public static void main(String args[]) {
		int arr[] = {14,15,23,6,20,78,32};
		System.out.println("Before sorted: ");
		System.out.println(Arrays.toString(arr));
		System.out.println("After sorted: ");
		Sort newArrSort = new Sort();
		arr = newArrSort.InsertionSort(arr);
		System.out.println(Arrays.toString(arr));
		System.out.printf("Sum of the array is: %d\n", newArrSort.CalculateSum(arr));
		System.out.printf("Average value of the array is: %.04f\n", newArrSort.FindAverage(arr));
	}

}
