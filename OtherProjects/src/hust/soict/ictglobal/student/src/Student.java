public class Student {
    private String studentID;
    private String studentName;
    private String birthday;
    private float gpa;

    public String getBirthday() {
        return birthday;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public float getGpa() {
        return gpa;
    }

    public void setGpa(float gpa) {
        this.gpa = gpa;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

}
