import java.util.Scanner;

public class StudentTest {

    public static String getBirthday(String birthday) throws IllegalBirthdayException {
        String[] tokens = birthday.split("[/]");
        if(tokens.length != 3){
            throw new IllegalBirthdayException("Wrong format!");

        }
        int dd = Integer.parseInt(tokens[0]);
        int mm = Integer.parseInt(tokens[1]);
        int yyyy = Integer.parseInt(tokens[2]);
        if(dd < 1 || dd > 31)
            throw  new IllegalBirthdayException("Wrong day. ( from 1- 31)");
        if(mm < 1 || mm > 12)
            throw  new IllegalBirthdayException("Wrong day. (from 1 - 12)");
        return birthday;

    }

    public static float getGPA(float gpa) throws IllegalGPAException {
        if (gpa < 0 || gpa > 4) {
            throw  new IllegalGPAException();
        }
        return gpa;
    }

    public static void main (String[] args) {
        Scanner scanner = new Scanner(System.in);
        Student s = new Student();

        System.out.println("Enter student's ID:");
        s.setStudentID(scanner.nextLine());
        System.out.println("Enter student's name:");
        s.setStudentName(scanner.nextLine());
        System.out.println("Enter student's birthday:");
        try {
            s.setBirthday(getBirthday(scanner.nextLine()));

        } catch (IllegalBirthdayException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

        System.out.println("Enter student's GPA:");
        try {
            s.setGpa(getGPA(scanner.nextFloat()));

        } catch (IllegalGPAException e) {
            System.err.println("ERROR: GPA must be >= 0 and <= 4");
        }

        scanner.close();
    }

}
