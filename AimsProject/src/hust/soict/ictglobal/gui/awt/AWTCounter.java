package hust.soict.ictglobal.gui.awt;
import java.awt.*;
import java.awt.event.*;


public class AWTCounter extends Frame implements ActionListener, WindowListener {
	private Label lblCount;
	private TextField tfCount;
	private Button btnCount;
	private int count = 0;
	
	public AWTCounter() {
		setLayout(new FlowLayout());
		lblCount = new Label("Counter");
		lblCount.setForeground(Color.BLACK);
		add(lblCount);
		
		tfCount = new TextField(count + ", 10");
		tfCount.setEditable(false);
		add(tfCount);
		
		btnCount = new Button("Count");
		//btnCount.setBackground(Color.BLACK);
		btnCount.setForeground(Color.BLACK);
		add(btnCount);
		
		btnCount.addActionListener(this);
		addWindowListener(this);
		
		setTitle("AWT Counter");
		setSize(250,100);
		
		setVisible(true);
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AWTCounter app = new AWTCounter();
		

	}
	
	@Override
	public void actionPerformed(ActionEvent avt)
	{
		++ count;
		tfCount.setText(count + "");
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		System.exit(0);
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
