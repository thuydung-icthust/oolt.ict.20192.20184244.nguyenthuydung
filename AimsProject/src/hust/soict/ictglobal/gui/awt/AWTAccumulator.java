package hust.soict.ictglobal.gui.awt;

import java.awt.*;
import java.awt.event.*;




public class AWTAccumulator extends Frame implements ActionListener,WindowListener {

	private Label lblInput;
	private Label lblOutput;
	private TextField tfInput;
	private TextField tfOutput;
	private int sum = 0;
	
	public AWTAccumulator() {
		setLayout(new FlowLayout());
		
		lblInput = new Label("Enter an integer: ");
		lblInput.setForeground(Color.BLACK);
		add(lblInput);
		
		tfInput = new TextField(10);
		tfInput.setBackground(Color.red);
		add(tfInput);
		
		tfInput.addActionListener(this);
		lblOutput = new Label("The Accumulated Sum is: ");
		lblOutput.setForeground(Color.BLACK);
		add(lblOutput);
		
		tfOutput = new TextField(10);
		tfOutput.setEditable(false);
		add(tfOutput);
		
		setTitle("AWT Accumulator");
		setSize(350, 120);
		addWindowListener(this);
		setVisible(true);
		
		System.out.println(lblInput.toString());
		System.out.println(lblOutput.toString());
		System.out.println(tfInput.toString());
		System.out.println(tfOutput.toString());
		
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new AWTAccumulator();
		

	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		int numberIn = Integer.parseInt(tfInput.getText());
		sum += numberIn;
		tfInput.setText("");
		tfOutput.setText(sum + "");
		
	}
	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowClosing(WindowEvent e) {
		System.exit(0);
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
		
	}
	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
