package hust.soict.ictglobal.aims;
import java.util.*;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;
import hust.soict.ictglobal.aims.media.Track;
import hust.soict.ictglobal.aims.order.Order;

// THIS IS MODIFIED VERSION !

public class Aims {
	public static void showMenu() {
		System.out.println("\nOrder Management Application: ");
		
		System.out.println("--------------------------------");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of order");
		System.out.println("0. Exit");
		System.out.println("--------------------------------");
		System.out.println("Please choose a number: 0-1-2-3-4");
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String[] args) throws PlayerException {
		/*
		// TODO Auto-generated method stub
		
		MemoryDaemon memoryDaemon = new MemoryDaemon();
		Thread thread = new Thread(memoryDaemon);
		thread.setDaemon(true);
		thread.start();
		
		int choice = 9;
		@SuppressWarnings("resource")
		Scanner keyboard = new Scanner(System.in);
		Order cur_Order = null;

		while(choice != 0)
		{
			showMenu();
			choice = Integer.parseInt(keyboard.nextLine());
			switch (choice) {
			case 1: {
				cur_Order = new Order();
				break;
			}
			case 2:
			{
				Media item = null;
				int choice2 = 0;
				System.out.println("You wanna add 1.DigitalDisc/ 2.Book/ 3.CompactDisc ?");
				choice2 = Integer.parseInt(keyboard.nextLine());
				  // Create a Scanner object
				if(choice2 == 1)
				{
					System.out.println("Enter title: ");
				    String title = keyboard.nextLine();
				    System.out.println("Enter category: ");

				    String category = keyboard.nextLine();
				    System.out.println("Enter cost: ");

				    String coststr = keyboard.nextLine();
				    float cost = Float.parseFloat(coststr);
				    
				    System.out.println("Enter director: ");

				    String director = keyboard.nextLine();
				    
				    System.out.println("Enter length: ");

				    int length = Integer.parseInt(keyboard.nextLine());
				    
				    item = new DigitalVideoDisc(director, category, title, length, cost);
				    //item = (DigitalVideoDisc) item;
				    
					cur_Order.addMedia(item);
					System.out.println("Do you want to play this item (y/n)?");
					if(keyboard.nextLine().equals("y"))
					{
						((DigitalVideoDisc) item).play();
					}
											
				}
				else if(choice2 == 2)
				{
					System.out.println("Enter title: ");

				    String title = keyboard.nextLine();
				    System.out.println("Enter category: ");

				    String category = keyboard.nextLine();
				    System.out.println("Enter cost: ");
				    
				    String coststr = keyboard.nextLine();
				    float cost = Float.parseFloat(coststr);
				    System.out.println("Enter authors, separate by comma: ");

				    String authors_string = keyboard.nextLine();
				    StringTokenizer st = new StringTokenizer(authors_string, ",");

					// create ArrayList object
					List<String> elements = new ArrayList<String>();

					// iterate through StringTokenizer tokens
					while(st.hasMoreTokens()) {

						// add tokens to AL
						elements.add(st.nextToken());
					}
					
					item = new Book(title, category, cost, elements);
					
					cur_Order.addMedia(item);
				}
				else if(choice2 == 3)
				{
					System.out.println("Enter CD's title: ");

				    String title = keyboard.nextLine();
				    System.out.println("Enter category: ");

				    String category = keyboard.nextLine();
				    System.out.println("Enter cost: ");
				    
				    String coststr = keyboard.nextLine();
				    float cost = Float.parseFloat(coststr);
				    
				    System.out.println("Enter artist: ");

				    String artist = keyboard.nextLine();
				    
				    System.out.println("Start enter list of tracks: ");
				    int num = 0;
				    List<Track> tracks = new ArrayList<Track>();
				    
				    while(true)
				    {
				    	num++;
				    	System.out.println("-----Track "+ num+" -------");
				    	System.out.println("Enter track's title: ");

					    String track_title = keyboard.nextLine();
					    System.out.println("Enter length of track: ");
					    int track_length = Integer.parseInt(keyboard.nextLine());
					    Track newtrack = new Track(track_title, track_length);
					    if(tracks.indexOf(newtrack) <0)
					    	tracks.add(newtrack);
					    else 
					    {
					    	System.out.println("Existed "+ newtrack.getTitle());
					    }
					    
					    System.out.println("Continue ( y/n)?");
					    if(keyboard.nextLine().equals("n"))
					    	break;
					    
				    }
				    
				    item = new CompactDisc(title, category, cost, artist, tracks);
			
				    cur_Order.addMedia(item);
				    System.out.println("Do you want to play this item (y/n)?");
					if(keyboard.nextLine().equals("y"))
					{
						((CompactDisc) item).play();
					}
				}
					
					
				    break;
				    
				
				
			    
			}
			case 3:
			{
				int choice3 = 0;
				System.out.println("Enter id of item you wanna remove: ");
				choice3 =Integer.parseInt(keyboard.nextLine());
				cur_Order.removeMedia(choice3);
				break;
			}
			case 4:
			{
				cur_Order.ListOrder();
			}
				
			}
			
		}
		
		
		/*
		Order orderbill2 = new Order();
		
		//Create a list of object.
		
		DigitalVideoDisc[] discs = new DigitalVideoDisc[5];
		discs[0] = new DigitalVideoDisc(null, null, "Conan", 13, (float) 24.45);
		discs[1] = new DigitalVideoDisc("James King", "Sience Fiction", "Iron Man", 12, 45.09f);
		discs[2] = new DigitalVideoDisc("Northern Side", 34.7f);
		discs[3] = new DigitalVideoDisc(null, "Romantic", "Love, Roise", 24, 89.9f);
		discs[4] = new DigitalVideoDisc("Inception", 32.6f);
		
		
		orderbill2.addMedia(discs);
		orderbill2.ListOrder();
		System.out.println(orderbill2.totalCost());
		//orderbill2.printList();
		
		// Get lucky item and set cost to 0. 
		
		Media lucky_item = orderbill2.getALuckyItem();
		//orderbill2.printList();
		
		System.out.println("The lucky item is "+lucky_item.getTitle());
		System.out.println(orderbill2.totalCost());
		
		*/
		DigitalVideoDisc disc1, disc2, disc3, disc4, disc5, disc6;
		disc1 = new DigitalVideoDisc(null, null, "Conan", 13, (float) 24.45);
		disc2 = new DigitalVideoDisc("James King", "Sience Fiction", "Iron Man", 12, 45.09f);
		disc3 = new DigitalVideoDisc("Northern Side", 34.7f);
		disc4 = new DigitalVideoDisc(null, "Romantic", "Love, Roise", 24, 89.9f);
		disc5 = new DigitalVideoDisc("Inception", 32.6f);
		disc6 = new DigitalVideoDisc("Inception", 20.1f);
		
		
		
		List<Media> collection = new ArrayList<Media>();
		collection.add(disc1);
		collection.add(disc2);
		collection.add(disc3);
		collection.add(disc4);
		collection.add(disc5);
		collection.add(disc6);
		
		for(Media disc: collection)
		{
			try {
				((DigitalVideoDisc)disc).play();
			}
			catch (PlayerException e) {
				// TODO: handle exception
				e.getMessage();
				e.toString();
				e.printStackTrace();
			}
			// ((DigitalVideoDisc)disc).play();
		}
		
		Iterator<Media> iterator = collection.iterator();
		System.out.println("--------------------");
		System.out.println("The DVDs currently in the order are: ");
		
		while(iterator.hasNext())
		{
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
			
		}
		
		// Sort the collection of DVDs - based on the compare to().
		
		Collections.sort((List) collection);
		iterator = collection.iterator();
		System.out.println("--------------------");
		System.out.println("The DVDs in sorted order are: ");
		
		while(iterator.hasNext())
		{
			DigitalVideoDisc temp = ((DigitalVideoDisc)iterator.next());
			
			System.out.println(temp.getTitle());
			System.out.println(temp.getCost());
			
		}
		System.out.println("--------------------");
		
		// Test Track.
		Track track1 = new Track("Lippo1", 9);
		Track track2 = new Track("Aippo1", 3);
		Track track3 = new Track("Lippo1", 4);
		Track track4 = new Track("Mippo1", 6);
		
		List<Track> tracks = new ArrayList<Track>();
		tracks.add(track1);
		tracks.add(track2);
		tracks.add(track3);
		tracks.add(track4);
		
		Iterator<Track> iterator1 = tracks.iterator();
		System.out.println("--------------------");
		System.out.println("The tracks currently in the order are: ");
		
		while(iterator1.hasNext())
		{
			System.out.println(((Track)iterator1.next()).getTitle());
			
		}
		
		// Sort the collection of DVDs - based on the compare to().
		
		Collections.sort((List) tracks);
		iterator1 = tracks.iterator();
		System.out.println("--------------------");
		System.out.println("The tracks in sorted order are: ");
		
		while(iterator1.hasNext())
		{
			Track temp = ((Track)iterator1.next());
			
			System.out.println(temp.getTitle());
			System.out.println(temp.getLength());
			
		}
		System.out.println("--------------------");
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
