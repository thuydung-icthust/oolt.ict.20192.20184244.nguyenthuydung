package hust.soict.ictglobal.aims.media;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import hust.soict.ictglobal.aims.PlayerException;



public class CompactDisc extends Disc implements Playable, Comparable{
	
	private String artist;
	private int length;
	private List<Track> tracks = new ArrayList<Track>();
	
	public CompactDisc(String title) {
		super(title);	
	}
	public CompactDisc(String title, String category)
	{
		super(title, category);
	}
	public CompactDisc(String title, String category, float cost)
	{
		super(title, category, cost);
	}
	public CompactDisc(String title, String category, float cost, String artist)
	{
		super(title, category, cost);
		this.artist = artist;
	}
	public CompactDisc(String title, String category, float cost, String artist, List<Track> tracks)
	{
		this(title, category, cost, artist);
		this.tracks = tracks;
		this.length = this.getLength();
		
	}
	
	public void play() throws PlayerException {
		if(this.getLength() <= 0) {
			System.err.println("ERROR: CD length is 0.");
			throw (new PlayerException());
			
		}
		System.out.println("Playing CD: " + this.getTitle());
		System.out.println("CD length: " + this.getLength());
		
		Iterator<Track> iter = tracks.iterator();
		Track nextTrack;
		
		while(iter.hasNext()) {
			nextTrack = (Track) iter.next();
			try {
				nextTrack.play();
			} catch (PlayerException e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			}
		}
	
	
	
	
	
	public String getArtist() {
		return artist;
	}
	public void setArtist(String arstist) {
		this.artist = arstist;
	}
	public int getLength() {
		length = 0;
		for(Track track: tracks)
		{
			length += track.length;
		}
		return length;
	}
	public void setLength(int length) {
		this.length = length;
		
	}
	public List<Track> getTracks() {
		return tracks;
	}
	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}
	
	public void addTrack(Track track)
	{
		if(tracks.indexOf(track) >= 0)
			tracks.add(track);
		else 
		{
			System.out.println(track.getTitle()+" has already existed in the tracks.");
		}
	}
	public void removeTrack(Track track)
	{
		if(tracks.indexOf(track) < 0)
		{
			System.out.println(track.getTitle()+" doesn't exits in the tracks.");
		}
		else {
			tracks.remove(track);
		}
	}
	
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		Track me = (Track) o;
		if(((Integer) this.tracks.size()).compareTo(tracks.size()) == 0)
		{
			return (((Integer)this.getLength()).compareTo(me.getLength()));	
		}
		else 
			return ((Integer) this.tracks.size()).compareTo(tracks.size());
			
	}
	

}
