package hust.soict.ictglobal.aims.media;

@SuppressWarnings("rawtypes")
public abstract class Media implements Comparable{

	protected String title;
	protected String category;
	protected float cost;
	
	
	public String getTitle() {
		
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	
	
	
	public Media(String title) {
		this.title = title;
		// TODO Auto-generated constructor stub
	}
	
	public Media(String title, String category)
	{
		this.title = title;
		this.category = category;
	}
	
	public Media(String title, String category, float cost)
	{
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	
	public int compareTo(Object o)
	{
		Media media = (Media) o;
		if(this.getTitle() == media.getTitle())
			return(((Float)this.getCost()).compareTo(media.getCost()));
		
		return(this.getTitle().compareTo(media.getTitle()));
		
	}
	
	@Override
	public boolean equals(Object obj) {
		
		try {
			Media ob = (Media) obj;
			if (this.getTitle() == ob.getTitle() && this.getCost() == ob.getCost())
				return true;
		}
		catch (NullPointerException e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		catch (ClassCastException e) {
			e.printStackTrace();
		}
		
		return false;
		
	}
	
	
}
