package hust.soict.ictglobal.aims.media;
import java.util.Scanner;

import hust.soict.ictglobal.aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable, Comparable {

		
	private String director;
	private int length;
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
	
	
	public DigitalVideoDisc(String title) {
		super(title);
		
	}
	
	public DigitalVideoDisc(String title, String category) {
		super(title, category);
	}
	public DigitalVideoDisc(String director, String category, String title) {
		super(title, category);
		this.director = director;
	}
	
	public DigitalVideoDisc(String director, String category, String title, int length, float cost) {
		super(title, category, cost);
		this.director = director;
		this.length = length;
		
	}
	public DigitalVideoDisc(String title, float cost) {
		super(title);
		this.cost = cost;
	}
	
	
	public boolean search(String title) {
		//ArrayList<String> list_titles = new ArrayList<String>();
		
		@SuppressWarnings("resource")
		Scanner token_title = new Scanner(title);
		while(token_title.hasNext())
		{
			String buffer = token_title.next();
			//System.out.println(buffer);
			if(title.toLowerCase().contains(buffer.toLowerCase()) == false)
			{
				//System.out.println("False!");
				return false;
			}
			
		}
		return true;
		
	}
	@Override
	public void play() throws PlayerException {
		if(this.getLength() <= 0)
		{
			System.err.println("ERROR: DVD length is 0.");
			throw(new PlayerException());
		}
		// TODO Auto-generated method stub
		System.out.println("Playing DVD: "+ this.getTitle());
		System.out.println("DVD length: "+ this.getLength());
		
		
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		DigitalVideoDisc objDisc = (DigitalVideoDisc) o;
		
		if(this.getTitle().compareTo(objDisc.getTitle()) == 0)			
			return ((Float)this.getCost()).compareTo((Float)objDisc.getCost());
		return this.getTitle().compareTo(objDisc.getTitle());
		
	}
}
	
	

	

