package hust.soict.ictglobal.aims.media;

public class Disc extends Media{
	
	public Disc(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}
	
	public Disc(String title, String category)
	{
		super(title, category);
	}
	
	public Disc(String title, String category, float cost)
	{
		super(title, category, cost);
	}
	
	protected int length;
	protected String direcotor;
	
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getDirecotor() {
		return direcotor;
	}
	public void setDirecotor(String direcotor) {
		this.direcotor = direcotor;
	}
	

}
