package hust.soict.ictglobal.aims.media;

import java.util.*;


public class Book extends Media implements Comparable{
	
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens;
	private Map<String, Integer> wordFrequency;
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		processContent();
	}

	public List<String> getContentTokens() {
		return contentTokens;
	}

	public void setContentTokens(List<String> contentTokens) {
		this.contentTokens = contentTokens;
	}

	public Map<String, Integer> getWordFrequency() {
		return wordFrequency;
	}

	public void setWordFrequency(Map<String, Integer> wordFrequency) {
		this.wordFrequency = wordFrequency;
	}

	
	public Book(String title, float cost, String content)
	{
		super(title);
		this.cost = cost;
		this.processContent();
	}
	public Book(String title, String category, float cost, List<String> authors) {
		super(title, category, cost);
		this.authors = authors;
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	public void addAuthor(String authorName) {
		if(this.authors.indexOf(authorName) < 0)
			this.authors.add(authorName);
	}
	
	public void removeAuthor(String authorName)
	{
		if(this.authors.indexOf(authorName) >= 0)
			this.authors.remove(authorName);
		
	}

	@Override
	public int compareTo(Object o) {
		Book obj = (Book) o;
		
		if(this.title == obj.title)
			return 1;
		return 0;
	}
	
	public void processContent()
	{
		// Split and get content tokens.
		
		String[]tokens = this.content.split(" |,|\\.|\n");
        ArrayList<String> content_tokens = new ArrayList<>();
        Collections.addAll(content_tokens, tokens);
        
        Collections.sort(content_tokens);
        this.setContentTokens(content_tokens);
        
        // count the frequency of each token.
        Map<String, Integer> count_freq = new HashMap<>();
        
        for(String str : content_tokens)
        {
        	int count = 0;
        	for(String str2 : content_tokens)
        	{
        		if(str.equals(str2))
        		{
        			count++;
        		}
        	}
        	count_freq.put(str, count);
        	
        }
        
        this.setWordFrequency(count_freq);
	}
	
	public void print()
	{
		System.out.printf("-----%s information----\n", this.getTitle());
		System.out.println("Category: "+ this.getCategory());
		System.out.println("Cost: "+ this.getCost());
		System.out.print("Author: ");
		for(String auth: this.getAuthors())
			System.out.printf("  %s, ", auth);
		System.out.println("\nLength: "+ this.getContentTokens().size()+" words.");
		
		this.wordFrequency.remove("");
		
		this.getWordFrequency().entrySet().forEach(entry->{
		    System.out.println(entry.getKey() + ": " + entry.getValue()+" times.");  
		 });
		
		
		
	}
	
	
	

}
