package hust.soict.ictglobal.aims.order;
import java.util.ArrayList;
import java.util.Date;

import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.media.Media;

public class Order {
	
	public static final int MAX_LIMITED_ORDERS = 5;
	private static int nOrders = 0;
	private Date dateOrdered;
	public static final int MAX_NUMBERS_ORDERED = 10;
	
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	
	
	public Date getDateOrdered() {
		return dateOrdered;
	}



	public void setDateOrdered(Date dateOrdered) {
		
		this.dateOrdered = dateOrdered;
	}


	
	public Order() {
		// TODO Auto-generated constructor stub
		if(nOrders >= MAX_LIMITED_ORDERS)
		{
			throw new ExceptionInInitializerError("Maximum order is 5, sorry!");
		}
		else {
			Date date = new Date();
			this.dateOrdered = date;
			nOrders++;
		}
		
	}
	
	

	public void addMedia(Media item) {
		if(itemsOrdered.size()+1 <= MAX_NUMBERS_ORDERED)
			itemsOrdered.add(item);
		else {
			System.out.println("can't add "+ item.getTitle()+ " because full of items.!");
		}
		}
		
	
	
	public void addMedia(Media... itemList) {
		
		
		for(Media item: itemList) 
		{
			if(itemsOrdered.size()+1 <= MAX_NUMBERS_ORDERED)
			{
			itemsOrdered.add(item);
			}
			else
			{
				System.out.println("Can't add "+ item.getTitle()+" because of full order.");
			}
		}
		
	}
	
	
	public void removeMedia(Media item) {
		if(itemsOrdered.indexOf(item) >= 0)
			itemsOrdered.remove(item);
		else
		{
			System.out.println(item.getTitle()+" not exits.");
		}
		
		//System.out.println("Can't find the item.");
	}
	public void removeMedia(int id)
	{
		if(id >= 0 && id < itemsOrdered.size())
			itemsOrdered.remove(id);
		else
		{
			System.out.println("Not exits.");
		}
	}
	
	public float totalCost()
	{
		float total =0 ;
		//qtyOrderNow = qtyOrdered();
		for(int i = 0; i< itemsOrdered.size(); i++)
			total += itemsOrdered.get(i).getCost();
		
		return total;
			
	}
	
	public void ListOrder() {
		System.out.println("This order includes: ");
		for(int i = 0; i< itemsOrdered.size(); i++)
			System.out.println(itemsOrdered.get(i).getTitle());
			
	}
	
	/*
	public void printList() {
		System.out.println("***************Order***************");
		System.out.printf("Date: [%s]\n", dateOrdered);
		for(int i = 0; i< itemsOrdered.size(); i++)
		{
			System.out.printf("%d. DVD - [%s] - [%s] - [%s] - [%d] : [%.4f]\n", i+1,itemsOrdered.get(i).getTitle(),
					itemsOrdered.get(i).getCategory(), itemsOrdered.get(i).getDirector(), itemsOrdered.get(i).getLength(), itemsOrdered[i].getCost());
			
		}
		
		System.out.println("Total cost: "+ this.totalCost());
		System.out.println("***********************************");
	}
	*/
	
	// pick a random item for free.
	public  Media getALuckyItem() {
		int lucky_index = (int) (Math.random() * itemsOrdered.size());
		itemsOrdered.get(lucky_index).setCost(0f);
		System.out.println("You've got a lucky item: "+itemsOrdered.get(lucky_index).getTitle());
		return itemsOrdered.get(lucky_index); 
		
	}
	
	
}	