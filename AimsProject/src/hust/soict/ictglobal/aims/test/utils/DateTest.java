package hust.soict.ictglobal.aims.test.utils;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;


public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		MyDate date1 = new MyDate();
		date1.print();
		
		MyDate date2 = new MyDate("October 11th 2000");
		date2.print();
		
		MyDate date3 = new MyDate(22, 10, 2020);
		date3.print();
		
		MyDate date4 = new MyDate();
		date4.accept();
		
		
	}

}

class MyDate {
	
	int day, year;
	int month;
	//String DAY, MONTH, YEAR;
	
	
	
	public MyDate() {
		// TODO Auto-generated constructor stub
		Date today = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(today);
		
		this.day = cal.get(Calendar.DAY_OF_MONTH);
		this.month = cal.get(Calendar.MONTH);
		this.year = cal.get(Calendar.YEAR);
		
		
		
	}
	
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		if(day> 0 && day <= 31)
		this.day = day;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if(month >= 1 && month <=12)
		this.month = month;
	}

	public MyDate(int day, int month, int year) {
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(String d_m_y) {
		String[] result = d_m_y.split(" ");
		//System.out.println(result);
		setYear(Integer.parseInt(result[2]));
		setDay(Integer.parseInt(result[1].replaceAll("[^\\d.]", "")));
		if(result[0].indexOf("Jan") != -1) this.month = 1;
		if(result[0].indexOf("Feb") != -1) this.month = 2;
		if(result[0].indexOf("Mar") != -1) this.month = 3;
		if(result[0].indexOf("Apr") != -1) this.month = 4;
		if(result[0].indexOf("May") != -1) this.month = 5;
		if(result[0].indexOf("Jun") != -1) this.month = 6;
		if(result[0].indexOf("Jul") != -1) this.month = 7;
		if(result[0].indexOf("Aug") != -1) this.month = 8;
		if(result[0].indexOf("Sep") != -1) this.month = 9;
		if(result[0].indexOf("Oct") != -1) this.month = 10;
		if(result[0].indexOf("Nov") != -1) this.month = 11;
		if(result[0].indexOf("Dec") != -1) this.month = 12;
	}
	
	public void accept() {
		String dayString;
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System. in);
        
		System.out.println("Enter your date in the form dd/mm/yyyy: ");
		dayString = scanner.nextLine();
		String[] result = dayString.split("/");
		//this(Integer.parseInt(result[0]), Integer.parseInt(result[1]), Integer.parseInt(result[2]));
		setDay(Integer.parseInt(result[0]));
		setMonth(Integer.parseInt(result[1]));
		setYear(Integer.parseInt(result[2]));
		
		this.print();
	}
	
	public void print() {
		System.out.printf("Current date is: %d/%d/%d\n", day, month, year);
	}
	
}